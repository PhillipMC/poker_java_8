package main.java.org.poker.util;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import main.java.org.poker.api.core.Deck;

import main.java.org.poker.api.core.Card;

public class CardUtil4Testing {
	
	private static final Map<String, Card> STRING_TO_CARD = Deck.getAllCards().stream().collect(Collectors.groupingBy(Card::toString, Collectors.reducing(null, (c, t) -> t)));
	
	
	
	private CardUtil4Testing() {}
	
	public static Card fromString(String s) {
		Card result = null;
		if(s != null) {
			result = STRING_TO_CARD.get(s);
		}
		return result;
	}
	
	
	public static Card[] fromStringCards(String s) {
		
		
		StringTokenizer st = new StringTokenizer(s);
		Card[] result = new Card[st.countTokens()];
		int i = 0;
		while (st.hasMoreTokens()) {
			result[i++] = fromString(st.nextToken());
		}
		return result;
	}
}
