package main.java.org.poker.util.statemachine;

@FunctionalInterface
public interface IChecker<T> {
	
	public boolean check(T context);

}
