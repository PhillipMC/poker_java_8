package main.java.org.poker.util;

import java.text.MessageFormat;

public class ExceptionUtil {
	
    public static final String MIN_VALUE_ERR_MSG = "El argumento {0} no puede tener un valor inferior a <{1}> y tiene <{2}>.";
	public static final String NULL_ERR_MSG = "El argumento {0} no puede ser nulo.";
	public static final String LENGTH_ERR_MSG = "El argumento {0} no puede ser nulo y debe tener una longitud de {1}.";
	
	private ExceptionUtil() {}
	
	public static void checkMinValueArgument(int o, int minValue, String name) {
		if(o < minValue) {
			throw new IllegalArgumentException(MessageFormat.format(MIN_VALUE_ERR_MSG, name, minValue, o));
		}
	}
	
	public static void checkMinValueArgument(long o, long minValue, String name) {
		if(o < minValue) {
			throw new IllegalArgumentException(MessageFormat.format(MIN_VALUE_ERR_MSG, name, minValue, o));
		}
	}
	
	public static void checkNullArgument(Object o, String name) {
		if (o == null)
			throw new IllegalArgumentException(MessageFormat.format(NULL_ERR_MSG, name));
	}
	
	public static<T> void checkArrayLengthArgument(T[] a, String name, int l) {
		if (a == null || a.length != 1) {
			throw new IllegalArgumentException(MessageFormat.format(LENGTH_ERR_MSG, name, 1));
		}
	}
	
	public static void checkArgument(boolean throwEx, String msg, Object... args) {
		if(throwEx) {
			throw new IllegalArgumentException(MessageFormat.format(msg, args));
		}		
	}
}
