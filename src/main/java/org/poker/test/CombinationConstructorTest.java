package main.java.org.poker.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

import main.java.org.poker.util.Combination;

public class CombinationConstructorTest {

	@Test
	public void testConstructor() {
		System.out.println("Combination(2,4)");
		int subItems = 2;
		int items = 4;
		long expectCombinations = 6L;
		Combination instance = new Combination(subItems, items);
		assertEquals(expectCombinations, instance.size());
		assertEquals(subItems, instance.size());
	}

	@Test//JUNIT4 (expected = IllegalArggumentException.class)
	public void testConstructorSubItemError() {
		Assertions.assertThrows(IllegalArgumentException.class, new Executable() {
			
			@Override
			public void execute() throws Throwable {
			System.out.println("Combination(0,1)");
			int subItems = 0;
			int items = 1;
			Combination instance = new Combination(subItems, items);
			}
		});
	}
	
	
	@Test//JUNIT4 (expected = IllegalArggumentException.class)
	public void testConstructorItemError() {
		Assertions.assertThrows(IllegalArgumentException.class, new Executable() {
		
			@Override
			public void execute() throws Throwable {
				System.out.println("Combination(5,1)");
				int subItems = 5;
				int items = 1;
				Combination instance = new Combination(subItems, items);
			}
		});
	}
	
	

}
