package main.java.org.poker.test;

import static org.junit.Assert.assertEquals;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import main.java.org.poker.api.core.HandEvaluator;
import main.java.org.poker.api.core.IHandEvaluator;
import main.java.org.poker.util.CardUtil4Testing; 


public class HandEvaluatorSteps {
	private static final String[] VALORES = {"mano0", "iguales", "mano1"};
	private IHandEvaluator handEvaluator;
	private String resultado;
	
	@Given("^un IHandEvaluator$")
	public void un_IHandEvaluator() throws Throwable {
		handEvaluator = new HandEvaluator();
	}
	
	@When("^calculamos la comparacion entre (.*) y (.*)$")
	public void calculamos_la_comparacion(String hand0, String hand1) throws Throwable {
		int evalhand0 = handEvaluator.eval(CardUtil4Testing.fromStringCards(hand0));
		int evalhand1 = handEvaluator.eval(CardUtil4Testing.fromStringCards(hand1));
		int diferencia = evalhand1 - evalhand0;
		if (diferencia != 0) {
			diferencia = Math.abs(diferencia) / diferencia;
		}
		resultado = VALORES[diferencia + 1];
	}
	
	@Then("^el resultado esperado es (.*)$")
	public void el_resultado_esperado_es(String expResult) throws Throwable {
		assertEquals(expResult, resultado);
	}
}
