package main.java.org.poker.dispatcher;


import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class GameEvent {
	
	private String type;
	private String source;
	private Object payload;
	
	public GameEvent() {}
	
	public GameEvent(String type, String source) {
		this.source = source;
		this.type = type;
	}
	
	public GameEvent(String type, String source, Object payload) {
		this.source = source;
		this.payload = payload;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Object getPayload() {
		return payload;
	}

	public void setPayload(Object payload) {
		this.payload = payload;
	}	

}
