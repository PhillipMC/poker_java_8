package main.java.org.poker.dispatcher;

public interface IGameEventDispatcher extends Runnable {
	
	public void dispatch(GameEvent event);
	public void exit();
} 
