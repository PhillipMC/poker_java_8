package main.java.org.poker.api.game;

import java.util.List;

import main.java.org.poker.api.core.Card;

public interface IStrategy {
	
	public String getName();
	
	public BetCommand getCommand(GameInfo<PlayerInfo> state);
	
	public default void updateState(GameInfo<PlayerInfo> state) {}
	
	public default void check(List<Card> communityCards) {}
	
	public default void onPlayerCommand(String player, BetCommand betCommand) {}

}
