package main.java.org.poker.api.game;

import main.java.org.poker.util.ExceptionUtil;
import main.java.org.poker.util.TexasHoldEmUtil.BetCommandType;

public class BetCommand {
	
	private final BetCommandType type;
	private long chips;
	
	public BetCommand(BetCommandType type, long chips) {
		ExceptionUtil.checkNullArgument(type, "type");
		ExceptionUtil.checkMinValueArgument(chips, 0L, "chips");
		this.type = type;
		this.chips = chips;
	}
	
	public BetCommand(BetCommandType type) {
		this(type, 0);
	}
	
	public BetCommandType getType() {
		return type;
	}
	
	public long getChips() {
		return chips;
	}
	
	public void setChips(long chips) {
		this.chips = chips;
	}

}
