package main.java.org.poker.api.core;

public interface IHandEvaluator {
	public int eval(Card[] cards);
}
